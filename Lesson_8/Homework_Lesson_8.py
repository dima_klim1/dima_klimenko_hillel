# Task 1

def change_type_value_to(value: type):
    """
    This parameterized decorator that takes as its argument the desired return type.
    """

    def decorator(func):
        def wrapper(arg):
            value_wrapper = func(arg)
            try:
                return value(value_wrapper)
            except ValueError:
                return None

        return wrapper

    return decorator


@change_type_value_to(str)
def change_to_str(x: str):
    return x


@change_type_value_to(int)
def change_to_int(x: str):
    return x


@change_type_value_to(bool)
def change_to_bool(x: str):
    return x


test_value = 1

res = change_to_str(test_value)
print(type(res))

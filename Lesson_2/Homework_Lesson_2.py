# Homework_Lesson_2


age_input = input('Enter your age: ')

if age_input.isdigit():
    age = int(age_input)
    if age == 0:
        print(f'you entered an invalid value "{age}"')
    elif age_input.startswith('0', 0):
        print('You entered an invalid value (zero at the beginning)')
    elif '7' in age_input:
        print('You are lucky today')
    elif age < 7:
        print('Where are your parents')
    elif 7 <= age < 16:
        print('This is an adult movie')
    elif age >= 120:
        print('how are you still alive?')
    elif age > 65:
        print('Show your retirement passport')
    else:
        print('No tickets')
else:
    print('Wrong input')

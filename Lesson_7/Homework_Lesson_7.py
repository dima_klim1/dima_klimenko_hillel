# Homework


def get_user_age(message):
    while True:
        try:
            age = int(input(message))
        except:
            print('Wrong data')
        else:
            break
    return age


def check_correct_age_word(age_str):
    word = ''
    age_str = str(age_str)
    roky_match = ('2', '3', '4')
    rokiv_match = ('5', '6', '7', '8', '9', '0')
    decyatki_match = ('11', '12', '13', '14')
    for Checker in decyatki_match:
        if age_str == Checker:
            word = 'років'
            return word
    for Checker in roky_match:
        if age_str[-1] in Checker:
            word = 'роки'
            return word
    for Checker in rokiv_match:
        if age_str[-1] in Checker:
            word = 'років'
            return word
    if age_str[-1] == '1':
        word = 'рік'
        return word


def check_user_age(age):
    if age == 0:
        print(f'you entered an invalid value "{user_age}"')
    elif '7' in str(age):
        print(f'Вам {user_age} {correct_age_word}, вам пощастить')
    elif age < 7:
        print(f'Тобі ж {user_age} {correct_age_word}! Де твої батьки?')
    elif 7 <= age < 16:
        print(f'Тобі лише {user_age} {correct_age_word}, а це е фільм для дорослих!')
    elif age >= 120:
        print('Ви ще живі?')
    elif age > 65:
        print(f'Вам {user_age} {correct_age_word}? Покажіть пенсійне посвідчення!')
    else:
        print(f'Незважаючи на те, що вам {user_age} {correct_age_word}, білетів всеодно нема!')
    return age


user_age = get_user_age('What is your age? ')
correct_age_word = check_correct_age_word(user_age)
age_checker = check_user_age(user_age)

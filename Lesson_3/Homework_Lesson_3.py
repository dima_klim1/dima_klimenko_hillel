# Task 1

input_word = input('Enter your word: ')
input_number = input('Enter the required character index: ')
special_characters = '[@_!#$%^&*()<>?/\|}{~:]'

if any(c in special_characters for c in input_word):
    print('You enter special character in to \'Word\'')
elif any(b.isdigit() for b in input_word):
    print('You enter number in \'Word\'')
elif input_number.startswith('0', 0):
    print('You enter zero at the beginning')
else:
    try:
        input_number = int(input_number)
        print(f'The {input_number} symbol in {input_word} is \'{input_word[input_number - 1]}\'')
    except (ZeroDivisionError, ValueError, TypeError, IndexError) as e:
        print(f'Exception:{type(e)}, {e}')
    except:
        print('Error')
# Task 2

letter_o = 'oO'

while True:
    input_word = input('Enter word with \'o\' letter: ')
    if any(c in letter_o for c in input_word):
        print('You enter correct word')
        break
    else:
        print(f'{input_word} - not contain \'o\'')

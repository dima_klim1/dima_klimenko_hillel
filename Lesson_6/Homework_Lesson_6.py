import requests
from tabulate import tabulate

# Task 1

url = 'http://api.open-notify.org/astros.json'

response = requests.get(url)
response_json = response.json()
people_data = response_json["people"]
print(people_data)
print(tabulate(people_data, headers='keys', tablefmt='grid'))

# Task 2

city_name = input('Enter your sity -  ')
while True:
    try:
        url = f'https://api.openweathermap.org/data/2.5/weather?q={city_name}&appid=47503e85fabbabc93cff28c52398ae97&units=metric'
        response = requests.get(url)
        response_json = response.json()
        temperature = response_json["main"]
        wind = response_json["wind"]
    except:
        print('Wrong data')
        exit()
    else:
        break
actual_temperature = temperature.get('temp')
wind_speed = wind.get('speed')
print(f'In {city_name.capitalize()} city actual temperature is {actual_temperature} , wind speed is {wind_speed}, '
      f'take care yourself :)')

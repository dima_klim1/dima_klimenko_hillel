import copy

# Task 1

Lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
Lst2 = []

for i in Lst1:
    if type(i) == str:
        Lst2.append(i)

print(Lst2)

# Task 2

List = [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44]
List_copy = copy.deepcopy(List)
for i in List_copy:
    if i < 21 or i > 74:
        List.remove(i)

print(List)

# Task 3
count = 0
str_input = input('Enter your string: ').lower().split()
word_list = list(str_input)
for i in word_list:
    if i[-1] == 'o':
        count += 1

print(f'{count} words contain \'o\' at the end')
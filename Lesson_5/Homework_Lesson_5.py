# Task 1

str_input = input('Enter your string: ').lower().split()
vowel = set('aeiou')
result = 0
for word in str_input:
    count = 0
    for letter in word:
        if letter in vowel:
            count += 1
        else:
            count = 0
        if count == 2:
            result += 1
            break

print(f'{result} words contain 2 vowel in a row')

# Task 2

lower_limit = input('Enter Lower Limit: ')
upper_limit = input('Enter Upper Limit: ')
magazine_list_filter = list()
My_magazine_dict = {
    "cito": 47.999,
    "BB_studio": 42.999,
    "momo": 49.999,
    "main-service": 37.245,
    "buy.now": 38.324,
    "x-store": 37.166,
    "the_partner": 38.988,
    "sota": 37.720,
    "rozetka": 38.003
}

try:
    lower_limit = float(lower_limit)
    upper_limit = float(upper_limit)
except ValueError:
    print('You enter wrong value')
    exit()

for magazine in My_magazine_dict.items():
    if lower_limit < magazine[1] < upper_limit:
        magazine_list_filter.append(magazine[0])
print('match with :', str(magazine_list_filter).replace('[', '').replace(']', ""))

